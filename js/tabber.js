var tabber = function(buttonsWrapper, dataWrapper) {
	//INITIALIZING TABBER
	buttonsWrapper.children().addClass('tab');
	dataWrapper.children().addClass('tabData');
	
	//creating a new select element with similar properties
	var optionsList = [];
	buttonsWrapper.children().each(function(i,childNode) {
		var option = {
			tabberid: $(childNode).attr('data-tabber'),
			html: $(childNode).text()
		}
		optionsList.push(option);
	});
	
	var selectElement = $("<select></select>").attr('id','tabSelector').addClass('pull');
	optionsList.forEach(function(element) {
		selectElement.append("<option class='selectTab' value='"+element.tabberid+
			"'> " + element.html + " </option> ");
	});
	selectElement.insertAfter(buttonsWrapper);

	//Select the first child by default
	$('.tab').first().click();

}

//TAB BUTTONS HANDLER
$('.tab').click(function() {
	$('.tab.active').removeClass('active');
	$(this).addClass('active');
	changeTabData($(this).attr('data-tabber'));
	changeTabSelector($(this).attr('data-tabber'));
});

//SELECTOR HANDLER
$(document).on('change','#tabSelector',function() {
	changeTabData($(this).val());
	changeTabButton($(this).val());
});

var changeTabData = function(tabName) {
	//UPDATE THE TAB ELEMENTS
	$('.tabData').hide();
	$('.tabData[data-tabber="' + tabName + '"]').show();
}

// CHANGE THE TAB BUTTON IF SELECTOR WAS CLICKED
var changeTabButton = function(tabName) {
	$('.tab.active').removeClass('active');
	$('.tab[data-tabber="' + tabName + '"]').addClass('active');
}

//CHANGE THE TAB SELECTOR IF BUTTON WAS CLICKED.
var changeTabSelector = function(tabName) {
	$('#tabSelector').val(tabName);
}